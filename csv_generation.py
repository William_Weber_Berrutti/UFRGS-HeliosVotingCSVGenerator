#!/bin/python

# csv_generation: randomly generates CSV files for testing for Helios 
# Voting system
# Copyright (C) 2024  William Weber Berrutti

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import csv
import logging
import os
import random
import sys
import string


# Global variables and configs
logging.basicConfig(level=logging.INFO)

csvFilesDir = "files"
validPrintableChars = string.digits + string.ascii_letters + string.punctuation.replace(',', '') + ' ' + '\t'


# Auxiliary functions
def saveFile(textToWrite = "", filePath = "", fileEncoding = "UTF8"):
    """
    Saves a string to a file.

    Keyword Arguments:
        textToWrite -- content to write on the text file (default: {""})
        filePath -- path to file (absolute or relative) (default: {""})
        fileEncoding -- file encoding (default: {"UTF8"})
    """
    
    if (textToWrite == ""):
        logging.error("Empty text content!")
        return
    
    if (filePath == ""):
        logging.error("Invalid filename path!")
        return
    
    with open(filePath, "w", encoding = fileEncoding) as f:
        f.write(textToWrite)
    
    if (os.path.exists(filePath)):
        logging.info(f"File '{filePath}' saved successfully!\n")


def generateRandomText(length = 0, validChars = validPrintableChars):
    """
    Generates a random text based on parameters, with repetition of characters.

    Keyword Arguments:
        length -- String size to generate (default: {0})
        validChars -- String that contains all valid chars to be randomly selected (default: {validPrintableChars})

    Returns:
        A string with randomly generated chars
    """
    
    return "".join(random.choice(validChars) for x in range(length))


def generateRandomCardCode():
    """
    Generates a random text based on parameters, used for card codes, which have 8 digits.
    
    Returns:
        A string with randomly generated digits
    """
    
    return generateRandomText(length = 8, validChars = string.digits)


# File Generation Test Functions
def generateCharacterAmountPerLine():
    """
    Generates a CSV file with a long line of characters (8 digits + 500 random chars for email + 500 random chars for person's name + 2 delimiters = 1010 total)
    """
    
    generatedText = f"{generateRandomCardCode()},{generateRandomText(480, string.digits + string.ascii_letters)}@myFictionalMail.com,{generateRandomText(500)}"
    
    saveFile(generatedText, f"{csvFilesDir}/character_amount_per_line.csv")


def generateSpecialCharacters(fileEncoding = "UTF8", fileName = "special_characters.csv"):
    """
    Generates a string with special characters.

    Keyword Arguments:
        fileEncoding -- File encoding (default: {"UTF8"})
        fileName -- Name of the file (default: {"special_characters.csv"})
    """
    
    generatedText = f"{generateRandomCardCode()},{validPrintableChars}@myFictionalMail.com,{validPrintableChars}"
    
    saveFile(generatedText, f"{csvFilesDir}/{fileName}", fileEncoding)


def generateLineAmount_NoSpecialChars_40Characters(textBlockMultiplier, textBlockRowAmount = 0):
    """
    Generates files with different amounts of rows, with 40 characters for both Name and Email, by multiples of textBlockRowAmount, up to textBlockMultiplier*textBlockRowAmount
    """
    
    generatedText = ""
    
    for i in range(textBlockMultiplier):
        for j in range(textBlockRowAmount):
            generatedText += f"{5*i+j:08},{generateRandomText(20, string.digits + string.ascii_letters)}@myFictionalMail.com,{generateRandomText(40, string.digits + string.ascii_letters)}\n"
        
        saveFile(generatedText, f"{csvFilesDir}/rows_amount_{(i+1)*(j+1)}.csv")


def main():
    logging.info(f"Starting CSV files generation...\n")
    
    generateCharacterAmountPerLine()
    generateSpecialCharacters()
    generateLineAmount_NoSpecialChars_40Characters(10, 5_000)
    generateLineAmount_NoSpecialChars_40Characters(10, 100_000)
    
    logging.info(f"Finished CSV files generation!\n")


if __name__ == "__main__":
    main()
