# UFRGS - Helios Voting CSV Generator

This repository contains functions that were created in Python to generate test CSV files to check the system's behavior. Some of the CSV files were manually generated. The others were generated with random contents and the following properties:

**File Encoding:** UTF-8

**Cell Separator:** Comma

**CSV Fields:**

- *Card Code:* positive integer (including zero)
- *E-mail:* string
- *Person name:* string


The following test scenarios were used as a base for CSV generation:

**1 - Blank Information:** Test whether with blank information in each cell it will be able to load, with the file having 3 lines.

Objective: Test system reaction with blank data.


**2 - Number of Characters per Line:** Test the total number of characters per line, with 8 characters for column 1 and 500 characters for columns 2 and 3 plus 2 commas separating fields, totaling 1010 characters.

**Objective:** Test system reaction with large line sizes.


**3 - Repeated information:** Have 1 file with 2 lines repeating only the email and have 1 file with 2 identical lines.

**Objective:** Test system reaction with repeated data.


**4 - Number of Lines:** Have CSVs of varying sizes (starting with 5k going up to 50k by multiples of 5k and jumping to 100k, then generating by multiples of 100k up to 1kk, generating 20 files in total).

**Objective:** Test system reaction with large volume of information.


**5 - Special Characters:** Test texts with special characters for all columns. Generate 1 line.

**Objective:** Test system reaction with data containing special characters.


# Dependencies

This project requires these Python modules installed in your system:

- csv
- logging
- os
- random
- sys
- string


# Usage

NOTE: For some tests, you have to edit both 'repeated_real_email.csv' and 'repeated_real_email_one_field.csv' to have real emails to check if the system will send duplicate emails for the same address.

Just run the `csv_generation.py` and wait until it generates all the files. When the `main` function finishes, it should have generated all the remaining necessary files for testing.


# Licensing

UFRGS - Helios Voting CSV Generator project is licensed under the [GPL-3](http://www.gnu.org/licenses/gpl.html).
